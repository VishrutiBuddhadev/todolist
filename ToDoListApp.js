let addbutton=document.getElementById('addbtn');
let usergiventask=document.getElementById('userinput');
let tasks=[];
let errMsg= document.getElementById('error');
let taskListContainer=document.getElementById('task_list');
addbutton.addEventListener('click',()=>{
    if(usergiventask.value.trim()=="" )
    {
    //alert('Please enter a valid Task');
    errMsg.innerHTML='Please enter a valid Task';
    errMsg.style.color="red";
    errMsg.style.fontSize='15px';
    }
    else{
        errMsg.innerHTML="";
        tasks.push(usergiventask.value);
        taskListContainer.innerHTML=""; 
        displaytasks(tasks);
        usergiventask.value="";   //to make textbox empty after adding any task
       }
})

// function addtask()
// {
//     if(usergiventask.value.trim()=="" )
//     {
//     //alert('Please enter a valid Task');
//     errMsg.innerHTML='Please enter a valid Task';
//     }
//     else{
//         errMsg.innerHTML="";
        
//         tasks.push(usergiventask.value);
//         displaytasks(tasks);
//         usergiventask.value="";   //to make textbox empty after adding any task
       
//     }
// }
function displaytasks(taskarray)
{
    
    tasks.forEach((task,index) => {  //index is used to access index to delete li element from the array
        
                                
        let lst=document.createElement('li');
        
        lst.innerHTML=task;
        lst.classList.add('lstele');
        lst.addEventListener('click',()=>{
        // lst.classList.add('taskcompleted');
        lst.classList.toggle('taskcompleted');
        })
        let icon=document.createElement('i');   //create icon
        icon.classList.add('fa-solid','fa-trash');
        icon.onclick=()=>{
            
            //access the index of the list element
                //alert(index)
            //delete it from array
            tasks.splice(index,1);  //1 indicates that remove 1 ele from index
            //display remaining tasks
            
            taskListContainer.innerHTML="";   //duplicates will be displayed if we don't do this
            displaytasks();
        }

        let divcontainer=document.createElement('div');
        divcontainer.classList.add('todotask');
        divcontainer.appendChild(lst);
        divcontainer.appendChild(icon);
        taskListContainer.appendChild(divcontainer);
        
        

        
    });
    console.log(tasks);

}

